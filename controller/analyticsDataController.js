var analyticsDataSchema = require("../model/analyticsDataSchema");
var searchDataSchema = require("../model/searchDataSchema");
var userSchema = require("../model/userSchema");

/**
  @route GET /analyticsData
  @desc analyticsData testing Route.
  @access Private
**/

exports.testingRoute = (req, res, next) => {
  res.json({ message: "AnalyticsData testing api" })
}

/**
  @route POST /analyticsData/checkKeyword
  @desc checkKeyword Route.
  @access Private
**/

exports.checkKeywordRoute = (req, res, next) => {
  const { searchKeyword } = req.body;

  if (!searchKeyword) {
    return res.json({ message: "SearchKeyword must be required", status: 0 });
  }

  userSchema.findOne({ email: req.user.email })
    .then((user) => {
      if (!user) return res.json({ message: "User not valid , try again", status: 0 })
      analyticsDataSchema.findOne({ searchKeyword })
        .then((findKeyWord) => {
          if (findKeyWord) {
            searchDataSchema.findOne({ searchKeyword, searchByUser: user._id })
              .then((isMatch) => {
                if (!isMatch) {
                  var newSearchData = new searchDataSchema({ searchKeyword, searchByUser: user._id, searchAnalyticsData: findKeyWord._id })
                  newSearchData.save()
                }
              })
            res.json({
              message: "success",
              status: 1,
              data: {
                isApiData: false,
                data: findKeyWord
              }
            })
          }
          else {
            res.json({
              message: "success",
              status: 1,
              data: {
                isApiData: true,
              }
            })
          }
        })
        .catch(() => {
          res.json({ message: "Internal server error!", status: 0 })
        })

    })
    .catch(() => {
      res.json({ message: "Internal server error!", status: 0 })
    })

}

/**
  @route POST /analyticsData/createData
  @desc createData Route.
  @access Private
**/

exports.createDataRoute = (req, res, next) => {
  const { analyticsData, searchKeyword, checkedStarted, checkedEnd, durationBetweenTime } = req.body;

  if (!analyticsData) {
    return res.json({ message: "Analytics data must be required", status: 0 });
  }

  if (!searchKeyword) {
    return res.json({ message: "SearchKeyword must be required", status: 0 });
  }

  if (!checkedStarted) {
    return res.json({ message: "checkedStarted must be required", status: 0 });
  }

  if (!checkedEnd) {
    return res.json({ message: "checkedEnd must be required", status: 0 });
  }

  if (!durationBetweenTime) {
    return res.json({ message: "durationBetweenTime must be required", status: 0 });
  }


  var newData = new analyticsDataSchema({ analyticsData, searchKeyword, checkedStarted, checkedEnd, durationBetweenTime });
  userSchema.findOne({ email: req.user.email })
    .then((user) => {
      if (!user) return res.json({ message: "User not valid , try again", status: 0 })
      analyticsDataSchema.findOne({ searchKeyword })
        .then((findKeyWord) => {
          if (findKeyWord) return res.json({ message: "SearchKeyword already exists", status: 0 })
          var newSearchData = new searchDataSchema({ searchKeyword, searchByUser: user._id, searchAnalyticsData: newData._id })
          newSearchData.save()
            .then(() => {
              newData.save()
                .then((find) => {
                  res.json({
                    message: "success",
                    status: 1,
                    data: {
                      data: find
                    }
                  })
                })
                .catch(() => {
                  res.json({ message: "Internal server error!", status: 0 })
                })
            })
        })
        .catch(() => {
          res.json({ message: "Internal server error!", status: 0 })
        })
    })
    .catch(() => {
      res.json({ message: "Internal server error!", status: 0 })
    })
}


/**
  @route GET /analyticsData/fetchData
  @desc fetchData Route.
  @access Private
**/

exports.fetchDataRoute = (req, res, next) => {

  userSchema.findOne({ email: req.user.email })
    .then((user) => {
      if (!user) return res.json({ message: "User not valid , try again", status: 0 })
      analyticsDataSchema.find()
        .then((data) => {
          res.json({ message: "success", status: 1, data })
        })
    })
    .catch(() => {
      res.json({ message: "Internal server error!", status: 0 })
    })
}

/**
  @route GET /analyticsData/fetchDataUserID
  @desc fetchDataByUserID Route.
  @access Private
**/

exports.fetchDataByUserIDRoute = (req, res, next) => {
  userSchema.findOne({ email: req.user.email })
    .then((user) => {
      if (!user) return res.json({ message: "User not valid , try again", status: 0 })
      searchDataSchema.find({ searchByUser: user._id }).
        populate('searchAnalyticsData').
        exec(function (err, data) {
          if (err) return res.json({ message: "Internal server error!", status: 0 })
          res.json({ message: "success", status: 1, data })
        })
    })
    .catch(() => {
      res.json({ message: "Internal server error!", status: 0 })
    })
}


/**
  @route Delete /analyticsData/deleteUserSearchData
  @desc deleteUserSearchData Route.
  @access Private
**/

exports.deleteUserSearchDataRoute = (req, res, next) => {
  const { searchKeyword } = req.body;

  if (!searchKeyword) {
    return res.json({ message: "searchKeyword must be required", status: 0 });
  }
  userSchema.findOne({ email: req.user.email })
    .then((user) => {
      if (!user) return res.json({ message: "User not valid , try again", status: 0 })
      searchDataSchema.findOneAndDelete({ searchByUser: user._id, searchKeyword })
        .then(() => {
          res.json({ message: "Deleted successfully", status: 1 })
        })
        .catch(() => {
          res.json({ message: "Internal server error!", status: 0 })
        })
    })
    .catch(() => {
      res.json({ message: "Internal server error!", status: 0 })
    })
}






